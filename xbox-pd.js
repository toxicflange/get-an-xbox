
const puppeteer = require('puppeteer');

(async () => {
  // Change to  headless: true to run without the browser showing up
  const browser = await puppeteer.launch({
    args:['--no-sandbox'],
    headless:false,
    sloMo: 100,
  });


  const page = await browser.newPage()
  // set the viewport so we know the dimensions of the screen
  await page.setViewport({ width: 1280, height: 1080 })
  const navigationPromise = page.waitForNavigation({ waitUntil: ['networkidle2'] })

  // Go to cart page, hit the login button
  await page.goto('http://xbox.com/en-CA/cart', {waitUntil: 'networkidle2'})
  await page.click('#mectrl_main_trigger')
  await navigationPromise 

  // login button takes you to login.live.com or something like that.. enter your creds.
  await page.waitForSelector('[name="loginfmt"]')
  console.log("waiting for Login form ... ")
  // Change to your username or grab from env variable
  await page.type('[name="loginfmt"]', "Username@address.com")
  await page.waitForTimeout(100)
  await page.click('[type="submit"]')
  await page.waitForNavigation({waitUntil:'domcontentloaded'}) // experimenting, not sure if this better or worse
  await page.waitForTimeout(100)                             // seems to work well enough
  console.log("UserID sent, waiting for Password form (200 OK) ... ")
  // we need to use waitForResponse because we are dealing with AJAX - no page navigation
  await page.waitForResponse(response => response.status() === 200)
  await navigationPromise           // this is new, not sure if this will ruin the timing
  await page.waitForTimeout(500)    // needs some time to render
  console.log("waiting for password selector")
  await page.waitForSelector('input[type="password"]', { visible: true })
  // Change to your password or grab from env variable
  await page.type('input[type="password"]', "hunter2")
	await page.keyboard.press('Enter')
  await navigationPromise
  await page.waitForTimeout(5000)

  // wait for checkout button
  console.log("waiting for checkout button")
  const frameSelector = "#BodyContent > div > iframe";
  await page.waitForSelector(frameSelector);
  await page.waitForTimeout(5000)
  // This is stupid because of frames and selectors, wasted too much time trying to 
  // make it work the 'right' way. It still works :D
  for (let i = 0; i <= 17; i ++) {
    await page.keyboard.press('Tab')
  }
  await page.keyboard.press('Enter')
  await navigationPromise
  await page.waitForTimeout(5000)

  // Loop until unable to purchase error goes away
  // Place Order
  for (let i = 0; i <= 38; i ++) {
    await page.waitForTimeout(100)
    await page.keyboard.press('Tab')
  }
  await page.keyboard.press('Enter')
  await navigationPromise
  await page.waitForTimeout(5000)

  const alert="#ember412 > div"
  let err = await page.$$(alert) // scoping may be wrong

  // Loop while you can't place an order
  while(err) {
    let err=undefined;
    console.log("No Xbox :( sleep and loop") 
    for (let i = 0; i <= 10; i ++) {
      await page.keyboard.press('Tab')
    }
    await page.keyboard.press('Enter')
    await navigationPromise
    await page.waitForTimeout(4000) // 4 seconds to give time to render

    const alert="#ember412 > div"
    err = await page.$$(alert)
  
    await page.waitForTimeout(60000) // 60 seconds to retry
  }

  await page.screenshot({ path: './XBoxOrder.jpg', type: 'jpeg' });
  console.log("No err !!  Happy day")

  await page.waitForTimeout(10000)
  await browser.close()
})()
